import shutil
import getpass
from binascii import a2b_base64
from distutils.dir_util import copy_tree
import urllib
from flask import jsonify, Response, request
from pathlib import Path
import os
import sys
sys.path.append("./utils")
import TIS
from app import app
import cv2
import json
import time
import sys
#sys.path.append("./utils")
#import TIS
from shutil import copy2, copytree


class VideoCamera(object):
    def __init__(self):
        # Get real-time video stream through opencv
        # url source see my last blog
        self.video = cv2.VideoCapture(0)
        if not self.video:
            print
            "!!! Failed VideoCapture: unable to open device 0"

    def __del__(self):
        print('kill kill kill')
        try:
            self.video.release()
        except:
            print('probably there\'s no cap yet :(')
        cv2.destroyAllWindows()

    def release(self):
        print('kill kill kill')
        try:
            self.video.release()
        except:
            print('probably there\'s no cap yet :(')
        cv2.destroyAllWindows()

    def get_frame(self, is_crop='False'):
        success, frame = self.video.read()
        frame = cv2.flip(frame, -1)
        if success:
            with open('app/static/settings.json') as json_file:
                dd = json.load(json_file)
            if is_crop == 'True':
                d = dd['crop']
                frame = frame[int(d['y']):int(d['y']) + int(d['h']), int(d['x']):int(d['x'] + d['w'])]
        ret, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()

    def take_picture(self, name, is_crop=False):
        success, frame = self.video.read()
        frame = cv2.flip(frame, -1)
        if is_crop:
            with open('app/static/settings.json') as json_file:
                dd = json.load(json_file)
            d = dd['crop']
            frame = frame[int(d['y']):int(d['y']) + int(d['h']), int(d['x']):int(d['x'] + d['w'])]
        cv2.imwrite(name + '.jpg', frame)
        return Response(status=200)


cam = None

Tis = TIS.TIS()
# 28220430 = Z30 PMF, 31120610 = Z30 CIS
Tis.openDevice("28220430", 2560, 1440, "20/1", TIS.SinkFormats.BGRA, False)  # Z30


username = getpass.getuser()


@app.route('/', methods=['GET'])
@app.route('/index')
def index():
    return 'hello pmf'


def gen(camera, is_crop='False'):
    while True:
        frame = camera.get_frame(is_crop)
        # Use generator function to output video stream, the content type of each request output is image/jpeg
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@app.route('/video_feed', methods=['GET'])
def video_feed(s=False):
    global cam
    cam = VideoCamera()
    d = request.args.get('crop', default='False')
    print('stream', d)
    if s:
        cam.release()
    return Response(gen(cam, d),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/takeimage', methods=['POST', 'GET'])
def takeimage():
    global cam
    cam.take_picture('screen')
    return Response(status=200)


@app.route('/takeimage2', methods=['POST', 'GET'])
def takeimage2():
    global cam
    cam.take_picture('pos', True)
    return Response(status=200)


@app.route('/savezones', methods=['GET', 'POST'])
def saveZones():
    print(username)
    d = request.get_json()
    data = d['zones']
    img = d['imgg']
    folder = d['folder']
    z1, z2 = [], []
    with urllib.request.urlopen(img) as response:
        n_img = response.read()
    fd = open('store/' + folder + '/screen_canvas.png', 'wb')
    fd.write(n_img)
    fd.close()
    with open('app/static/settings.json') as json_file:
        dd = json.load(json_file)
    for a in dd['levels']:
        z1.append(a['z1'])
        z2.append(a['z2'])
    Path('store/' + folder).mkdir(parents=True, exist_ok=True)
    zones = {'zones': []}
    for i in range(len(data['x'])):
        z1_curr = z1[data['z'][i]]
        z2_curr = z2[data['z'][i]]
        zones['zones'].append({'id': i, 'name': data['name'][i], 'position': {'x1': int(data['x'][i]),
                                                                              'y1': int(data['y'][i]),
                                                                              'x2': int(data['w'][i]),
                                                                              'y2': int(data['h'][i]),
                                                                              'Lvl': data['z'][i],
                                                                              'z1': z1_curr, 'z2': z2_curr},
                               'type': data['type'][i]})
    with open('store/' + folder + '/zones.json', 'w', encoding='utf8') as outfile:
        json.dump(zones, outfile, ensure_ascii=False)
<<<<<<< HEAD
   # copytree('store/' + folder, '/home/pmfvision/Desktop/' + folder, dirs_exist_ok=True)
=======
    #shutil.copytree('store/' + folder, '/home/pmfvision/Desktop/' + folder)
    copytree('store/' + folder, '/home/pmfvision/Desktop/' + folder,dirs_exist_ok=True)
>>>>>>> master
    return Response(status=200)


@app.route('/save_scenario', methods=['POST'])
def saveScenario():
    d = request.get_json()
    data = d['scenario']
    folder = d['folder']
    Path('store/' + folder).mkdir(parents=True, exist_ok=True)
    with open('store/' + folder + '/scenario.json', 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)
    try:
        copy2('screen.jpg', 'store/' + folder)
    except IOError:
        os.chmod('app/', 777)  # ?? still can raise exception
        copy2('screen.jpg', 'store/' + folder)
    return Response(status=200)


@app.route('/reinit_img', methods=['GET'])
def reinit_img():
    try:
        copy2('screen.jpg', 'app/')
    except IOError:
        os.chmod('app/', 777)  # ?? still can raise exception
        copy2('screen.jpg', '/app')
    img = cv2.imread('screen.jpg')
    img[:] = (255, 255, 255)
    cv2.imwrite('screen.jpg', img)
    return Response(status=200)


@app.route('/load_img', methods=['GET'])
def load_img():
    global cam
    if cam != None:
        cam.release()
        cam = None
    folder = request.args.get('folder', '')
    os.remove('screen.jpg')
    try:
        copy2('store/' + folder + '/screen.jpg', os.curdir)
    except IOError:
        copy2('store/' + folder + '/screen.jpg', os.curdir)
<<<<<<< HEAD
  #  copytree('store/' + folder, '/home/pmfvision/Desktop/' + folder, dirs_exist_ok=True)
=======
    copytree('store/' + folder, '/home/pmfvision/Desktop/' + folder,dirs_exist_ok=True)
>>>>>>> master
    return Response(status=200)


@app.route('/duplproject', methods=['GET', 'POST'])
def duplProject():
    d = request.get_json()
    proj = d['proj']
    copytree('store/' + proj, 'store/' + proj + ' copy', dirs_exist_ok=True)
    return Response(status=200)


@app.route('/renproject', methods=['GET', 'POST'])
def renProject():
    d = request.get_json()
    o_name = d['o_name']
    n_name = d['n_name']
    os.rename('store/' + o_name, 'store/' + n_name)
    return Response(status=200)


@app.route('/readzones', methods=['GET'])
def readZones():
    folder = request.args.get('folder', '')
    mode = request.args.get('mode', '')
    zones = {}
    if mode == 'normal':
        with open('store/' + folder + '/zones.json') as json_file:
            data = json.load(json_file)
    if mode == 'global':
        with open('store/' + folder + '/zones_global.json') as json_file:
            data = json.load(json_file)
            data = data['zones']
    return jsonify(data)


@app.route('/readscenario', methods=['GET'])
def readScenario():
    folder = request.args.get('folder', '')
    zones = {}
    with open('store/' + folder + '/scenario.json') as json_file:
        data = json.load(json_file)
    return jsonify(data)


@app.route('/read_zones_ctrl', methods=['GET'])
def readZonesCtrl():
    folder = request.args.get('folder', '')
    first = request.args.get('first', '')
    print(first)
    if first == 'false':
        print(first)
        with open('store/' + folder + '/zones_views.json') as json_file:
            data = json.load(json_file)
            return jsonify(data)
    else:
        with open('store/' + folder + '/zones_global.json') as json_file:
            data = json.load(json_file)
            return str(len(data['zones']))


@app.route('/get_project_names', methods=['GET'])
def getProjectNames():
    names = os.listdir('store/')
    names = sorted(names, key=str.casefold)
    print('asdasd',names)
    return jsonify(names)


@app.route('/save_pos_zone', methods=['GET', 'POST'])
def savePosZone():
    d = request.get_json()
    # print(d)
    # data = {'crop': []}
    # data['crop'].append({
    #     "y": d['y'],
    #     "x": d['x'],
    #     "w": d['w'],
    #     "h": d['h']
    # })
    with open('app/static/settings.json', 'r') as f:
        json_data = json.load(f)
        json_data['crop'] = {"y": int(d['y']), "x": int(d['x']), "w": int(d['w']), "h": int(d['h'])}
    with open('app/static/settings.json', 'w', encoding='utf8') as outfile:
        json.dump(json_data, outfile, ensure_ascii=False)
    return Response(status=200)


@app.route('/save_crop', methods=['GET', 'POST'])
def saveCrop():
    global cam
    if cam != None:
        cam.release()
        cam = None
    d = request.get_json()
    folder = d['folder']
    file = d['file']
    img = d['imgg']
    os.chmod('store/' + folder, 0o0777)
    if img == '':
        return Response(status=200)
    with urllib.request.urlopen(img) as response:
        n_img = response.read()
    Path('store/' + folder + '/imgs').mkdir(parents=True, exist_ok=True)
    try:
        copy2('pos.jpg', 'store/' + folder + '/imgs')
    except IOError:  # ?? still can raise exception
        copy2('pos.jpg', 'store/' + folder + '/imgs')
    fd = open('store/' + folder + '/imgs/' + file + '.png', 'wb')
    fd.write(n_img)
    fd.close()
    os.rename('store/' + folder + '/imgs/pos.jpg', 'store/' + folder + '/imgs/' + file + '.jpg')
    copytree('store/' + folder, '/home/pmfvision/Desktop/' + folder,dirs_exist_ok=True)

    img = cv2.imread('pos.jpg')
    img[:] = (255, 255, 255)
    return Response(status=200)


@app.route('/remove_op', methods=['POST'])
def removeOp():
    d = request.get_json()
    folder = d['folder']
    file = d['file']
    os.chmod('store/' + folder, 0o0777)
    os.remove('store/' + folder + '/imgs/' + file + '.png')


@app.route('/save_zone_global', methods=['GET', 'POST'])
def saveZoneGlobal():
    d = request.get_json()
    data = d['zones']
    z = d['zoom']
    global_zones = {'zoom': z, 'zones': []}
    for i in range(len(data['x'])):
        global_zones['zones'].append({'id': i , 'zoom': data['zoom'][i], 'position': {'x1': int(data['x'][i]),
                                                            'y1': int(data['y'][i]),
                                                            'x2': int(data['w'][i]),
                                                            'y2': int(data['h'][i]),
                                                            }})
    print(global_zones)
    folder = d['folder']
    with open('store/' + folder + '/zones_global.json', 'w', encoding='utf8') as outfile:
        json.dump(global_zones, outfile, ensure_ascii=False)
    return Response(status=200)


@app.route('/save_zones_final', methods=['GET', 'POST'])
def saveZonesFinal():
    d = request.get_json()
    dd = d['view']
    print('adasd',len(dd['zones']))
    folder = d['folder']
    with open('store/' + folder + '/zones_global.json', 'r', encoding='utf8') as f:
        json_data = json.load(f)
        zones_glob = json_data['zones']
    view = {'name': 'Final control', 'global_img': 'control_scenarios/' + folder + '/imgs/global_img.jpg', 'zoom_level': json_data['zoom'],  'steps': []}
    for v in range(len(dd['zones'])):
        view['steps'].append({'id': v, 'name': 'Control step ' + str(v),'control_zoom_level': zones_glob[v]['zoom'], 'control_img': 'control_scenarios/control_example/imgs/step' + str(v)+'.jpg', 'zones': [],'global_coord': zones_glob[v]['position']})
        data = dd['zones'][v]
        print('quiopas',data)
        for i in range(len(data['x'])):
            view['steps'][v]['zones'].append({'id': i, 'position': {'x1': int(data['x'][i]),
                                                                              'y1': int(data['y'][i]),
                                                                              'x2': int(data['w'][i]),
                                                                              'y2': int(data['h'][i]),
                                                                              'type': data['type'][i],
                                                                              'expected_text': data['expected_text'][i],
                                                                              'instruction': data['instruction'][i]
                                                                              }})
    with open('store/' + folder + '/zones_views.json', 'w', encoding='utf8') as outfile:
        json.dump(view, outfile, ensure_ascii=False)
    return Response(status=200)


@app.route('/save_zone_control', methods=['GET', 'POST'])
def saveZoneCtrl():
    d = request.get_json()
    data = d['zone']
    ctr_zones = []
    for z in range(0, len(data['x1'])):
        print(z)
        ctr_zones.append(
            {'id': z, 'type': data['type'][z], 'x1': int(data['x1'][z]),
             'y1': int(data['y1'][z]), 'x2': int(data['x1'][z] + data['x2'][z]),
             'y2': int(data['y1'][z] + data['y2'][z])})
    print(ctr_zones)
    folder = d['folder']
    file = d['file']
    name = d['name']
    zoom = d['zoom']
    idd = d['id']
    print('name :', name)
    step = d['step']
    print('step :', step)
    print('id :', idd)
    zone = d['z']
    Path('store/' + folder).mkdir(parents=True, exist_ok=True)
    zones = {'zone': []}
    with open('store/' + folder + '/scenario.json', 'r') as infile:
        json_data = json.load(infile)
        for idx, j in enumerate(json_data['steps']):
            if j['id'] == int(step):
                for idx2, o in enumerate(j['operations']):
                    if o['id'] == int(idd):
                        json_data['steps'][idx]['operations'][idx2] = \
                            {'id': int(idd), 'zone': int(zone), 'action': 'control', 'name': name, 'ref_imgs': [

                                'scenarios/' + folder + '/imgs/' + file + '.jpg'], 'ref_display_img': 'scenarios/' + folder + '/imgs/' + file + '_dis.png',
                                 'ref_display_img_zoom': zoom, 'control_zones': ctr_zones}
    with open('store/' + folder + '/scenario.json', 'w',encoding='utf8') as outfile:

        json.dump(json_data, outfile, ensure_ascii=False)
    return Response(status=200)


@app.route('/save_display_image', methods=['POST', 'GET'])
def snap_display_img():
    global Tis
    import time
    d = request.get_json()
    folder = d['folder']
    file = d['file']
    os.chmod('store/' + folder, 0o0777)
    Path('store/' + folder + '/imgs').mkdir(parents=True, exist_ok=True)
    print('snap_display', d)
    image = Tis.Get_image()  # Get the image. It is a numpy array
    filename = 'store/' + folder + '/imgs'
    cv2.imwrite(os.path.join(filename, file + '_dis.png'), image)

    print("image saved")
    # Stop the pipeline and clean up
    return Response(status=200)


@app.route('/video_feed2', methods=['GET'])
def video_feed2():
    z = request.args.get('zoom', '')
    return Response(gen2(z),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


def gen2(z):
    global Tis

    # possible zoom values: 40, 50, 70
    Tis.Set_Property("Zoom", 40)
    Tis.Start_pipeline()
    Tis.Set_Property("FocusAuto", "Once")
    while True:
        if Tis.Snap_image(0.01) is True:
            image = Tis.Get_image()  # Get the image. It is a numpy array
            # Use generator function to output video stream, the content type of each request output is image/jpeg
            frame = cv2.imencode('.jpg', image)[1].tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@app.route('/video_zoom', methods=['GET', 'POST'])
def zoomTis():
    global Tis
    d = request.get_json()
    z = d['zoom']
    Tis.Set_Property("Zoom", z)
    Tis.Set_Property("FocusAuto", "Once")
    print('Zoomed ',z)
    return Response(status=200)
