import Vue from 'vue';
import VueRouter from 'vue-router';
import createScenario from '../components/create_scenario.vue';
import stream from '../components/stream.vue';
import Index from '../components/index.vue';
import ViewImg from '../components/view_img.vue';
import posImg from '../components/posImg.vue';
import globalView from '../components/ctrlFinal_globalView.vue';
import zonesFinal from '../components/ctrlFinal_ctrlPicts.vue';
import ctrlFInal from '../components/ctrlFinal_finalView.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'index',
    component: Index,
    alias: '/index',
  },
  {
    path: '/scenario/:load',
    name: 'Create_Scenario',
    component: createScenario,
  },
  {
    path: '/stream/:type/:project/:step/:pos/:name/:id/:zone',
    name: 'Stream',
    component: stream,
  },
  {
    path: '/view_pos_img/:project/:step/:pos/:name/:id/:zone/:zoom',
    name: 'Position image',
    component: posImg,
  },
  {
    path: '/global_view/:project/:zoom',
    name: 'Global view',
    component: globalView,
  },
  {
    path: '/zones_final/:project/:zoom',
    name: 'Zones final control',
    component: zonesFinal,
  },
  {
    path: '/view_img/:load',
    name: 'View_Img',
    component: ViewImg,
  },
  {
    path: '/ctrl_final/:project',
    name: 'Final view',
    component: ctrlFInal,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
