import Vue from 'vue';
import Vuelidate from 'vuelidate';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { BootstrapVue } from 'bootstrap-vue';
import App from './App.vue';
import 'bootstrap-icons/font/bootstrap-icons.css';
import router from './router';
import i18n from './i18n'

Vue.use(Vuelidate);
Vue.use(BootstrapVue);

Vue.config.productionTip = false;
Vue.prototype.$reload = false;

new Vue({
  router,
  i18n,
  render: (h) => h(App)
}).$mount('#app');
